import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-weather-details',
  templateUrl: './weather-details.component.html',
  styleUrls: ['./weather-details.component.css']
})
export class WeatherDetailsComponent implements OnInit {
  weather = <any>{}
  constructor(private location : Location) { }

  ngOnInit() {
    this.weather = JSON.parse(localStorage.getItem('weather'));
  }



}
