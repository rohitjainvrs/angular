import { Injectable } from '@angular/core';
import { WHEATHER_API_URL } from './const';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }
  getWeather(): Observable<any> {
    return this.http.get<any>(WHEATHER_API_URL)
  }
}
